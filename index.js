const MongoAtlas = require('./lib');
const UserRole = require('./lib/DatabaseUsers/UserRole');
const UserScope = require('./lib/DatabaseUsers/UserScope');
const USER_ROLE_NAME = require('./lib/Constant/USER_ROLE_NAME');
const SCOPE_TYPE = require('./lib/Constant/SCOPE_TYPE');

module.exports = {
  MongoAtlas,
  UserRole,
  UserScope,
  USER_ROLE_NAME,
  SCOPE_TYPE,
};
