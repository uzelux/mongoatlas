module.exports = {
  ADMIN: {
    ATLAS_ADMIN: 'atlasAdmin',
    READ_WRITE_ANY_DATABASE: 'readWriteAnyDatabase',
    READ_ANY_DATABASE: 'readAnyDatabase',
    CLUSTER_MONITOR: 'clusterMonitor',
    BACKUP: 'backup',
    DB_ADMIN_ANY_DATABASE: 'dbAdminAnyDatabase',
    ENABLE_SHARDING: 'enableSharding',
  },
  DATABASE: {
    DB_ADMIN: 'dbAdmin',
    READ: 'read',
    READ_WRITE: 'readWrite',
  },
  COLLECTION: {
    READ: 'read',
    READ_WRITE: 'readWrite',
  },
};
