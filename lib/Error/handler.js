module.exports = class ErrorHandler {
  constructor({
    logging = console,
  }) {
    this.logger = logging;
  }

  handle(error) {
    this.message = `MongoAtlas Error #${error.error}: ${error.detail}`;
    if (this.logger) {
      this.logger.log(this.message);
    }
    return this;
  }
};
