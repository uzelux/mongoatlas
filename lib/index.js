const MongoAtlas = require('./MongoAtlas');

const CreateDatabaseUser = require('./API/DatabaseUsers/CreateDatabaseUser');
const GetAllDatabaseUsers = require('./API/DatabaseUsers/GetAllDatabaseUsers');
const DeleteDatabaseUser = require('./API/DatabaseUsers/DeleteDatabaseUser');

Object.assign(MongoAtlas.prototype, {
  CreateDatabaseUser,
  GetAllDatabaseUsers,
  DeleteDatabaseUser,
});

module.exports = MongoAtlas;
