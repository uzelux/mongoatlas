module.exports = (queryObj) => Object.entries(queryObj).map(([key, val]) => `${key}=${val}`).join('&');
