const DigestFetch = require('digest-fetch');
const required = require('./util/required');
const baseURL = require('./Constant/BASE_URL');

module.exports = class MongoAtlas {
  /**
   * @param {string} publicKey
   * @param {string} privateKey
   * @param {string} [group]
   * @param {string} [db]
   * @param {Boolean} [pretty=false]
   * @param {Boolean} [envelope=false]
   */
  constructor({
    publicKey,
    privateKey,
    group,
    db,
    pretty = false,
    envelope = false,
  }) {
    if (!publicKey) required('publicKey');
    if (!privateKey) required('privateKey');
    this._API = new DigestFetch(
      publicKey,
      privateKey,
      { algorithm: 'MD5' },
    );
    this._uri = '';
    this._group = group;
    this._db = db;
    this._baseUrl = baseURL;

    this._pretty = pretty;
    this._envelope = envelope;
  }

  group(input) {
    if (!input) return this._group;
    this._group = input;
    return this;
  }

  db(input) {
    if (!input) return this._db;
    this._db = input;
    return this;
  }
};
