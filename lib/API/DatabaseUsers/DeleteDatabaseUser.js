const required = require('../../util/required');
const query = require('../../util/query');

/**
 * @param {Object} input
 * @param {string} [input.groupId=this._group]
 * @param {string} [input.databaseName=this._db]
 * @param {string} input.username
 * @return {Promise<Object>}
 */
module.exports = async function DeleteDatabaseUser({
  groupId = this._group,
  databaseName = this._db,
  username,
}) {
  if (!groupId) required('groupId');
  if (!databaseName) required('databaseName');
  if (!username)required('username');

  this._username = username;

  this._uri = `/groups/${groupId}/databaseUsers/${databaseName}/${username}`;
  this._query = {
    pretty: this._pretty,
    envelope: this._envelope,
  };
  await this._API.fetch(`${this._baseUrl}${this._uri}?${query(this._query)}`, {
    method: 'DELETE',
  });
  return ({ groupId, databaseName, username });
};
