const required = require('../../util/required');
const query = require('../../util/query');

/**
 * @param {Object} input
 * @param {string} [input.groupId=this._group]
 * @param {Number} [input.pageNum=1]
 * @param {Number} [input.itemsPerPage=100]
 * @param {Boolean} [input.includeCount=false]
 * @return {Promise<void>}
 */
module.exports = async function GetAllDatabaseUsers({
  groupId = this._group,
  pageNum = 1,
  itemsPerPage = 100,
  includeCount = true,
}) {
  if (!groupId) required('groupId');

  this._uri = `/groups/${groupId}/databaseUsers`;
  this._query = {
    pageNum,
    itemsPerPage,
    includeCount,
    pretty: this._pretty,
    envelope: this._envelope,
  };
  const res = await this._API.fetch(`${this._baseUrl}${this._uri}?${query(this._query)}`, {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
  });
  return res.json();
};
