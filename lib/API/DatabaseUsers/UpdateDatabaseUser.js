const required = require('../../util/required');
const query = require('../../util/query');

/**
 * @param {Object} input
 * @param {string} [input.groupId=this._group]
 * @param {Number} [input.databaseName=this._db]
 * @param {Number} [input.username=this._username]
 * @return {Promise<void>}
 */
module.exports = async function UpdateDatabaseUser({
  groupId = this._group,
  databaseName = this._db,
  username = this._username,
}) {
  if (!groupId) required('groupId');
  if (!databaseName) required('databaseName');
  if (!username) required('username');

  this._uri = `/groups/${groupId}/databaseUsers/${databaseName}/${username}`;
  this._query = {
    pretty: this._pretty,
    envelope: this._envelope,
  };
  const res = await this._API.fetch(`${this._baseUrl}${this._uri}?${query(this._query)}`, {
    method: 'PATCH',
    headers: { 'Content-Type': 'application/json' },
  });
  return res.json();
};
