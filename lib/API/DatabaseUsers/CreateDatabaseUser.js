const required = require('../../util/required');
const query = require('../../util/query');

const generatePassword = (length = 16) => {
  let output = '';
  let char = '';
  while (output.length < length) {
    output = `${output}${Math.random().toString(36).substring(2)}`;
  }
  while (char.length < length) {
    char = `${char}${Math.random().toString(2).substring(2)}`;
  }

  return output
    .slice(length * -1)
    .split('')
    .map((c, i) => (char[i] === '0' ? c : c.toUpperCase()))
    .join('');
};

/**
 * @param {Object} input
 * @param {string} [input.groupId=this._group]
 * @param {string} [input.databaseName=this._db]
 * @param {string} input.username
 * @param {string} input.password
 * @param {UserRole[]} input.roles
 * @param {UserScope[]} input.scopes
 * @return {Promise<void>}
 */
module.exports = async function CreateDatabaseUser({
  groupId = this._group,
  databaseName = this._db,
  username,
  password = generatePassword(),
  roles = [],
  scopes = [],
}) {
  if (!groupId) required('groupId');
  if (!databaseName) required('databaseName');
  if (!username)required('username');
  if (!password)required('password');
  if (roles.length === 0) throw new ReferenceError('No roles are configured');
  roles.forEach((role) => role.validate());
  scopes.forEach((scope) => scope.validate());

  this._username = username;
  this._password = password;

  this._uri = `/groups/${groupId}/databaseUsers`;
  this._data = {
    groupId,
    databaseName,
    username,
    password,
    roles: roles.map((role) => role.exec()),
  };
  this._query = {
    pretty: this._pretty,
    envelope: this._envelope,
  };
  if (scopes.length > 0) {
    Object.assign(this._data, { scopes: scopes.map((scope) => scope.exec()) });
  }
  const res = await this._API.fetch(`${this._baseUrl}${this._uri}?${query(this._query)}`, {
    method: 'POST',
    body: JSON.stringify(this._data),
    headers: { 'Content-Type': 'application/json' },
  });
  return res.json();
};
