const SCOPE = require('../Constant/SCOPE_TYPE');

module.exports = class UserScope {
  /**
   * @param {Object} [input]
   * @param {string} input.name
   * @param {string} input.type
   */
  constructor(input) {
    if (input) {
      const {
        name,
        type,
      } = input;
      this._name = name;
      this._type = type;
    }
  }

  name(input) {
    this._name = input;
    return this;
  }

  type(input) {
    this._type = input;
    return this;
  }

  validate() {
    if (!this._name) {
      throw new ReferenceError('No Scope name is configured');
    }

    if (this._type) {
      if (Object.values(SCOPE).includes(this._type)) return this;
      throw new ReferenceError(`${this._type} is not a valid scope type`);
    }
    throw new ReferenceError('No type configured');
  }

  isValid() {
    try {
      this.validate();
      return true;
    } catch (e) {
      return false;
    }
  }

  exec() {
    return { name: this._name, type: this._type };
  }
};
