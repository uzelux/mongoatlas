const ROLE = require('../Constant/USER_ROLE_NAME');

module.exports = class UserRole {
  /**
   * @param {Object} [input]
   * @param {string} input.collectionName
   * @param {string} input.databaseName
   * @param {string} input.roleName
   */
  constructor(input) {
    if (input) {
      const {
        collectionName,
        databaseName,
        roleName,
      } = input;
      this._collectionName = collectionName;
      this._databaseName = databaseName;
      this._roleName = roleName;
    }
  }

  collection(input) {
    this._collectionName = input;
    return this;
  }

  database(input) {
    this._databaseName = input;
    return this;
  }

  db(input) {
    return this._database(input);
  }

  role(input) {
    this._roleName = input;
    return this;
  }

  validate() {
    if (this._collectionName) {
      if (Object.values(ROLE.COLLECTION).includes(this._roleName)) return this;
      throw new ReferenceError(`${this._roleName} cannot be assigned to a collection`);
    }

    if (this._databaseName) {
      if (this._databaseName === 'admin' && Object.values(ROLE.ADMIN).includes(this._roleName)) return this;
      if (Object.values(ROLE.DATABASE).includes(this._roleName)) return this;
      throw new ReferenceError(`${this._roleName} cannot be assigned to this database ${this._databaseName}`);
    }
    throw new ReferenceError('No database configured');
  }

  isValid() {
    try {
      this.validate();
      return true;
    } catch (e) {
      return false;
    }
  }

  exec() {
    return {
      collectionName: this._collectionName,
      databaseName: this._databaseName,
      roleName: this._roleName,
    };
  }
};
