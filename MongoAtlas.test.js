/**
 * @jest-environment node
 */
require('dotenv').config();
const MongoAtlas = require('./lib');
const UserRole = require('./lib/DatabaseUsers/UserRole');
const UserScope = require('./lib/DatabaseUsers/UserScope');
const USER_ROLE_NAME = require('./lib/Constant/USER_ROLE_NAME');
const SCOPE_TYPE = require('./lib/Constant/SCOPE_TYPE');

const publicKey = process.env.PUBLIC_KEY;
const privateKey = process.env.ATLAS_PRIVATE_KEY;
const groupId = process.env.ATLAS_GROUP_ID;

const cluster = 'Service';

describe('User', () => {
  it.only('should retrieve user', async () => {
    const atlas = new MongoAtlas({
      publicKey,
      privateKey,
    });

    const result = await atlas.GetAllDatabaseUsers({ groupId });
    console.log({ result });
    expect(result).toBeTruthy();
  });
  it.only('should create user', async () => {
    const authenticateDatabase = 'admin';
    const username = 'test-user';

    const atlas = new MongoAtlas({
      publicKey,
      privateKey,
    });

    const result = await atlas.CreateDatabaseUser({
      groupId,
      databaseName: authenticateDatabase,
      username,
      roles: [
        new UserRole({
          roleName: USER_ROLE_NAME.DATABASE.DB_ADMIN,
          databaseName: 'test-db',
        }),
        new UserRole({
          roleName: USER_ROLE_NAME.DATABASE.READ_WRITE,
          databaseName: 'test-db',
        }),
      ],
      scopes: [
        new UserScope({
          name: cluster,
          type: SCOPE_TYPE.CLUSTER,
        }),
      ],
    });

    console.log({ result });

    expect(result).toBeTruthy();
    expect(result.groupId).toEqual(groupId);
    expect(result.username).toEqual(username);
    expect(result.roles.length).toEqual(2);
    expect(result.scopes.length).toEqual(1);
    expect(result.awsIAMType).toEqual('NONE');
    expect(result.ldapAuthType).toEqual('NONE');
    expect(result.x509Type).toEqual('NONE');
  });
});
